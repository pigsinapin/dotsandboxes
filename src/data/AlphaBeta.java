package data;

import static java.lang.Math.*;

import players.HardAI;

/**
 * Created by evand on 4/20/2017.
 */
public class AlphaBeta {

    public int alphabeta(GameBoardData gameData, int depth, int alpha, int beta, boolean maxPlayer) {
        if (depth == 0 || gameData.checkGameOver()) {
            return evalFunction(gameData);
        }
        if (maxPlayer){
            int v = -100000000;
            for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
                //gameData.FUNCTION.takeLine(item);
                v = max(v, alphabeta(gameData, depth - 1, alpha, beta, false));
                alpha = max(alpha, v);
                if (beta <= alpha) {
                    break;
                }
            }
            return v;
        }
        else {
            int v = 100000000;
            for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
                //gameData.FUNCTION.takeLine(item);
                v = min(v, alphabeta(gameData, depth - 1, alpha, beta, true));
                beta = min(beta, v);
                if (beta <= alpha) {
                    break;
                }
            }
            return v;
        }
    }


    /*
    public int max_value(GameBoardData gd, int alpha, int beta, int depth) {
        if (depth == 0 || gd.checkGameOver()) {
            return evalFunction(gd);
        }
        int v = -100000000;
        for (Line item : gd.GAMEBOARD.availableLines()) { // needs to be children
            //gameData.FUNCTION.takeLine(item);
            v = max(v, min_value(gd, depth - 1, alpha, beta));
            alpha = max(alpha, v);
            if (beta <= alpha) {
                break;
            }
        }
        return v;
    }

    public int min_value(GameBoardData gd, int alpha, int beta, int depth) {
        if (depth == 0 || gd.checkGameOver()) {
            return evalFunction(gd);
        }
        int v = 100000000;
        for (Line item : gd.GAMEBOARD.availableLines()) { // needs to be children
            //gameData.FUNCTION.takeLine(item);
            v = min(v, max_value(gd, depth - 1, alpha, beta));
            beta = min(beta, v);
            if (beta <= alpha) {
                break;
            }
        }
        return v;
    }

    public void alphabeta(GameBoardData gameData, int depth) {

    }
*/

    public int evalFunction(GameBoardData gameData) {
        int utility = 0;
        Coin[] coins = gameData.FUNCTION.coins;
        utility += gameData.currentPlayer.getPoints();

        System.out.printf("Player %d has %d utility\n", gameData.currentPlayer.PLAYERNUMBER, utility);
        return utility;
    }
}
