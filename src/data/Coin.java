package data;

/**
 * Created by evand on 4/14/2017.
 */
public class Coin {
    int owner;
    int ID;
    Coin north, east, south, west;
    boolean visited = false;

    public Coin(int id, Coin n, Coin e, Coin s, Coin w) {
        owner = 0;
        ID = id;
        north = n;
        east = e;
        south = s;
        west = w;
    }

    public void player1gets() {
        owner = 1;
    }

    public void player2gets() {
        owner = 2;
    }

    public void currentPlayerGets(int player) {
        owner = player;
    }

    public void setNorth(Coin n) {
        north = n;
    }

    public void setEast(Coin e) {
        east = e;
    }

    public void setSouth(Coin s) {
        south = s;
    }

    public void setWest(Coin w) {
        west = w;
    }

    public void cutNorth() {
        north = null;
    }

    public void cutEast() {
        east = null;
    }

    public void cutSouth() {
        south = null;
    }

    public void cutWest() {
        west = null;
    }

    public void printNorth() {
        if (north != null)
            System.out.print(" | ");
        else
            System.out.print("   ");
    }

    public void printEast() {
        if (east != null)
            System.out.print("-");
        else
            System.out.print(" ");
    }

    public void printSouth() {
        if (south != null)
            System.out.print(" | ");
        else
            System.out.print("   ");
    }

    public void printWest() {
        if (west != null)
            System.out.print("-");
        else
            System.out.print(" ");
    }

    public void printOwner() {
        System.out.print(owner);
    }

    public void printID() {
        System.out.printf("%2d", ID);
    }

    public int getNumNeighbors() {
        int neighbors = 0;
        if (north != null)
            ++neighbors;
        if (east != null)
            ++neighbors;
        if (south != null)
            ++neighbors;
        if (west != null)
            ++neighbors;
        return neighbors;
    }

    public int findNeighbor(int id) {
        if (this.north != null && this.north.ID == id) {
            //System.out.printf("north of %d is %d\n", this.ID, this.north.ID);
            return 1;
        }
        else if (this.east != null && this.east.ID == id) {
            //System.out.printf("east of %d is %d\n", this.ID, this.east.ID);
            return 2;
        }
        else if (this.south != null && this.south.ID == id) {
            //System.out.printf("south of %d is %d\n", this.ID, this.south.ID);
            return 3;
        }
        else {
            //System.out.printf("west of %d is %d\n", this.ID, this.west.ID);
            return 4;
        }
    }

    public void print() {
        if (north != null)
            System.out.println("  |  ");
        else
            System.out.println("     ");
        if (west != null)
            System.out.print("- ");
        else
            System.out.println("  ");
        System.out.print(ID);
        if (east != null)
            System.out.println(" -");
        else
            System.out.print("  ");
        if (south != null)
            System.out.println("  |  ");
        else
            System.out.println("     ");

    }
}
