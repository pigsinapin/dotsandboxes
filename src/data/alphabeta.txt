package data;

import static java.lang.Math.*;
import java.util.*;

import players.HardAI;

/**
 * Created by evand on 4/20/2017.
 */
public class AlphaBeta {
    /*
    public int alphabeta(GameBoardData gameData, int depth, int alpha, int beta, boolean maxPlayer) {
        if (depth == 0 || gameData.checkGameOver()) {
            return evalFunction(gameData);
        }
        if (maxPlayer){
            int v = -100000000;
            for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
                //gameData.FUNCTION.takeLine(item);
                v = max(v, alphabeta(gameData, depth - 1, alpha, beta, false));
                alpha = max(alpha, v);
                if (beta <= alpha) {
                    break;
                }
            }
            return v;
        }
        else {
            int v = 100000000;
            for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
                //gameData.FUNCTION.takeLine(item);
                v = min(v, alphabeta(gameData, depth - 1, alpha, beta, true));
                beta = min(beta, v);
                if (beta <= alpha) {
                    break;
                }
            }
            return v;
        }
    }
    */


    public int max_value(GameBoardData gameData, Coin[] coins, int cp, int alpha, int beta, int depth) {
        if (depth == 0 || gameData.checkGameOver()) {
            return evalFunction(cp, coins);
        }
        int v = -100000000;
        for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
            //gameData.FUNCTION.takeLine(item);
            //GameBoardData gd = gameData.clone();
            //Coin[] c = gameData.FUNCTION.coins.clone();
            Coin[] c = new Coin[26];
            for (Coin coin: coins) {
                if (coin.ID == -1) {
                    c[0] = new Coin(-1, null, null, null, null);
                }
                else {
                    c[coin.ID] = coin.clone();
                }
            }
            BoardState.cutString(c, cp, item.box1.getID(), item.box2.getID());
            v = max(v, min_value(gameData, c, toggleCurrentPlayer(cp), depth - 1, alpha, beta));
            if (v >= beta) {
                return v;
            }
            alpha = max(alpha, v);
            //if (beta <= alpha) {
            //    break;
            //}
        }
        return v;
    }

    public int min_value(GameBoardData gameData, Coin[] coins, int cp, int alpha, int beta, int depth) {
        if (depth == 0 || gameData.checkGameOver()) {
            return evalFunction(cp, coins);
        }
        int v = 100000000;
        for (Line item : gameData.GAMEBOARD.availableLines()) { // needs to be children
            //gameData.FUNCTION.takeLine(item);
            //GameBoardData gd = gameData.clone();
            //Coin[] c = gameData.FUNCTION.coins.clone();
            Coin[] c = new Coin[26];
            for (Coin coin: coins) {
                if (coin.ID == -1) {
                    c[0] = new Coin(-1, null, null, null, null);
                }
                else {
                    c[coin.ID] = coin.clone();
                }
            }
            BoardState.cutString(c, cp, item.box1.getID(), item.box2.getID());
            v = min(v, max_value(gameData, c, toggleCurrentPlayer(cp), depth - 1, alpha, beta));
            if (v <= alpha) {
                return v;
            }
            beta = min(beta, v);
            //if (beta <= alpha) {
            //    break;
            //}
        }
        return v;
    }

    public Line alphabeta(GameBoardData gameData, int depth) {
        //List<Line> successors = gameData.GAMEBOARD.availableLines();
        //Line l = successors.stream().max(min_value(gameData, -100000000, 100000000, 3)).get()
        int maxUtility = -1;
        Line toReturn = null;
        int currPlayer = gameData.currentPlayer.PLAYERNUMBER;
        //for (Box b: gameData.GAMEBOARD.availableBoxes()) {
        //    System.out.printf("Box %d\n", b.getID());
        //}

        Vector<Line> lines = gameData.GAMEBOARD.availableLines();
        Collections.shuffle(lines);

        //for (Line l : gameData.GAMEBOARD.availableLines()) {
        for (Line l : lines) {
            //gameData.FUNCTION.takeLine(l);
            //System.out.printf("boxes %d and %d\n", l.box1.getID(), l.box2.getID());
            //MethodHolder mh = gameData.FUNCTION.clone();
            //GameBoardData gd = gameData.clone();
            //BoxesArray gb = gameData.GAMEBOARD.clone();
            //System.out.println("cloned");
//            System.out.printf("Box %d\n", l.box1.getID());
//            System.out.printf("Box %d\n", l.box2.getID());

            //Coin[] c = new Coin[26];
            //c = gameData.FUNCTION.coins.clone();

            Coin[] c = new Coin[26];
            for (Coin coin : gameData.FUNCTION.coins) {
                if (coin.ID == -1) {
                    c[0] = new Coin(-1, null, null, null, null);
                }
                else {
                    c[coin.ID] = coin.clone();
                }
            }

            BoardState.cutString(c, currPlayer, l.box1.getID(), l.box2.getID());
//            BoardState.printCoins(c);
//            System.out.println();

            //currPlayer = toggleCurrentPlayer(currPlayer);
            int curr = min_value(gameData, c, currPlayer, -100000000, 100000000, depth);
            if (curr > maxUtility) {
                maxUtility = curr;
                toReturn = l;
            }

        }

        //return gameData.FUNCTION.gameBoard.availableLines().get(0);
        //return gameData.GAMEBOARD.availableLines().get(0);
        if (toReturn != null)
            return toReturn;
        else
            return gameData.GAMEBOARD.availableLines().get((int) random() % gameData.GAMEBOARD.availableLines().size());
    }

    public int toggleCurrentPlayer(int p) {
        if (p == 1) {
            return 2;
        }
        else {
            return 1;
        }
    }


    public int evalFunction(int cp, Coin[] coins) {
        int utility = 0;
        utility += BoardState.numCoinsOwned(cp, coins);
        //utility -= BoardState.numCoinsOwned(toggleCurrentPlayer(cp), coins); // compare points?
        utility += BoardState.thirdSide(coins);
//        if (utility > 0)
//            System.out.printf("%d points\n", utility);
        //Coin[] coins = gameData.FUNCTION.coins;
        //utility += gameData.currentPlayer.getPoints();

        //System.out.printf("Player %d has %d utility\n", gameData.currentPlayer.PLAYERNUMBER, utility);
        //BoardState.printCoins(coins);
        //System.out.printf("%d utility for player %d\n", utility, cp);
        //System.out.println();
        return utility;
    }
}
