package data;

/**
 * Created by evand on 4/14/2017.
 */
public class BoardState {

    public static Coin[] inializeStrings(Coin[] coins) {
        //coin 1
        coins[1].setNorth(coins[0]);
        coins[1].setEast(coins[2]);
        coins[1].setSouth(coins[6]);
        coins[1].setWest(coins[0]);
        //coin 2
        coins[2].setNorth(coins[0]);
        coins[2].setEast(coins[3]);
        coins[2].setSouth(coins[7]);
        coins[2].setWest(coins[1]);
        //coin 3
        coins[3].setNorth(coins[0]);
        coins[3].setEast(coins[4]);
        coins[3].setSouth(coins[8]);
        coins[3].setWest(coins[2]);
        //coin 4
        coins[4].setNorth(coins[0]);
        coins[4].setEast(coins[5]);
        coins[4].setSouth(coins[9]);
        coins[4].setWest(coins[3]);
        //coin 5
        coins[5].setNorth(coins[0]);
        coins[5].setEast(coins[0]);
        coins[5].setSouth(coins[10]);
        coins[5].setWest(coins[4]);
        //coin 6
        coins[6].setNorth(coins[1]);
        coins[6].setEast(coins[7]);
        coins[6].setSouth(coins[11]);
        coins[6].setWest(coins[0]);
        //coin 7
        coins[7].setNorth(coins[2]);
        coins[7].setEast(coins[8]);
        coins[7].setSouth(coins[12]);
        coins[7].setWest(coins[6]);
        //coin 8
        coins[8].setNorth(coins[3]);
        coins[8].setEast(coins[9]);
        coins[8].setSouth(coins[13]);
        coins[8].setWest(coins[7]);
        //coin 9
        coins[9].setNorth(coins[4]);
        coins[9].setEast(coins[10]);
        coins[9].setSouth(coins[14]);
        coins[9].setWest(coins[8]);
        //coin 10
        coins[10].setNorth(coins[5]);
        coins[10].setEast(coins[0]);
        coins[10].setSouth(coins[15]);
        coins[10].setWest(coins[9]);
        //coin 11
        coins[11].setNorth(coins[6]);
        coins[11].setEast(coins[12]);
        coins[11].setSouth(coins[16]);
        coins[11].setWest(coins[0]);
        //coin 12
        coins[12].setNorth(coins[7]);
        coins[12].setEast(coins[13]);
        coins[12].setSouth(coins[17]);
        coins[12].setWest(coins[11]);
        //coin 13
        coins[13].setNorth(coins[8]);
        coins[13].setEast(coins[14]);
        coins[13].setSouth(coins[18]);
        coins[13].setWest(coins[12]);
        //coin 14
        coins[14].setNorth(coins[9]);
        coins[14].setEast(coins[15]);
        coins[14].setSouth(coins[19]);
        coins[14].setWest(coins[13]);
        //coin 15
        coins[15].setNorth(coins[10]);
        coins[15].setEast(coins[0]);
        coins[15].setSouth(coins[20]);
        coins[15].setWest(coins[14]);
        //coin 16
        coins[16].setNorth(coins[11]);
        coins[16].setEast(coins[17]);
        coins[16].setSouth(coins[21]);
        coins[16].setWest(coins[0]);
        //coin 17
        coins[17].setNorth(coins[12]);
        coins[17].setEast(coins[18]);
        coins[17].setSouth(coins[22]);
        coins[17].setWest(coins[16]);
        //coin 18
        coins[18].setNorth(coins[13]);
        coins[18].setEast(coins[19]);
        coins[18].setSouth(coins[23]);
        coins[18].setWest(coins[17]);
        //coin 19
        coins[19].setNorth(coins[14]);
        coins[19].setEast(coins[20]);
        coins[19].setSouth(coins[24]);
        coins[19].setWest(coins[18]);
        //coin 20
        coins[20].setNorth(coins[15]);
        coins[20].setEast(coins[0]);
        coins[20].setSouth(coins[25]);
        coins[20].setWest(coins[19]);
        //coin 21
        coins[21].setNorth(coins[16]);
        coins[21].setEast(coins[22]);
        coins[21].setSouth(coins[0]);
        coins[21].setWest(coins[0]);
        //coin 22
        coins[22].setNorth(coins[17]);
        coins[22].setEast(coins[23]);
        coins[22].setSouth(coins[0]);
        coins[22].setWest(coins[21]);
        //coin 23
        coins[23].setNorth(coins[18]);
        coins[23].setEast(coins[24]);
        coins[23].setSouth(coins[0]);
        coins[23].setWest(coins[22]);
        //coin 24
        coins[24].setNorth(coins[19]);
        coins[24].setEast(coins[25]);
        coins[24].setSouth(coins[0]);
        coins[24].setWest(coins[23]);
        //coin 25
        coins[25].setNorth(coins[20]);
        coins[25].setEast(coins[0]);
        coins[25].setSouth(coins[0]);
        coins[25].setWest(coins[24]);
        return coins;
    }

    public static void cutString(Coin[] c, int id1, int id2) {
        if (id2 < 0) {
            if (id2 == -1) {
                c[id1].cutNorth();
            }
            else if (id2 == -2) {
                c[id1].cutEast();
            }
            else if (id2 == -3) {
                c[id1].cutSouth();
            }
            else {
                c[id1].cutWest();
            }
        }
        else {
            int cut1 = c[id1].findNeighbor(id2);
            //System.out.printf("%d is %d of %d\n", id2, cut1, id1);
            if (cut1 == 1)
                c[id1].cutNorth();
            else if (cut1 == 2)
                c[id1].cutEast();
            else if (cut1 == 3)
                c[id1].cutSouth();
            else
                c[id1].cutWest();
            int cut2 = c[id2].findNeighbor(id1);
            //System.out.printf("%d is %d of %d\n", id1, cut1, id2);
            if (cut2 == 1)
                c[id2].cutNorth();
            else if (cut2 == 2)
                c[id2].cutEast();
            else if (cut2 == 3)
                c[id2].cutSouth();
            else
                c[id2].cutWest();
        }
    }

    public static Coin[] makeCoinsandStrings() {
        Coin outterEdge = new Coin(-1, null, null, null, null);
        Coin[] coins = new Coin[26];
        coins[0] = outterEdge;
        int i;
        for (i = 1; i < 26; ++i) {
            coins[i] = new Coin(i, outterEdge, outterEdge, outterEdge, outterEdge);
        }
        coins = inializeStrings(coins);
        return coins;
    }

    public static void printCoins(Coin[] coins) {
        int i, j;
        for (i = 0; i < 5; ++i) {
            for (j = 1; j < 6; ++j) {
                coins[i*5 + j].printNorth();
            }
            System.out.println();
            for (j = 1; j < 6; ++j) {
                coins[i*5 + j].printWest();
                coins[i*5 + j].printOwner();
                coins[i*5 + j].printEast();
            }
            System.out.println();
            for (j = 1; j < 6; ++j) {
                coins[i*5 + j].printSouth();
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        //makeBoard();
        Coin[] coinBoard = makeCoinsandStrings();
        printCoins(coinBoard);
    }
}
